# Dalia Night Theme


A complete, dark and minimalistic Material-inspired theme.

Features:
- **Complete**: Every single themeable component has been customized.
- **Dark**: Darker than the default Monokai theme, only grayscale colors and accent colors are used.

## Install

Follow the instructions in the [Marketplace], or run the following in the command palette:

## Usage

- Run the `Preferences: Color Theme` command.
- Select `Dalia Night Theme`


MIT © Victor Jose

**Enjoy!**